FROM ubuntu:18.04

RUN mkdir /network && cd /network
VOLUME /network
WORKDIR /network

COPY chainSpec.json .
COPY install_parity.sh .
RUN apt update && \
    apt install -y curl sudo && \
    bash /network/install_parity.sh -r stable

EXPOSE 8545 30303 30301
CMD ["parity", "--chain", "/network/chainSpec.json"]
